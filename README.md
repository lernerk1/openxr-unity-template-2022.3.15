# Open XR Project Template for Unity version 2022.3.15

This project template assists in quick set up for Open XR unity projects. It includes a readme, a git ignore file designed for working in unity, and a git attributes file also designed for working in unity.

For help using this repo please look [**here**](https://gitlab.msu.edu/lernerk1/openxr-unity-template-2022.3.15/-/blob/main/GitLabHelp.md?ref_type=heads).

Thank you Jeremy Bond for creating and publishing the [**original repo**](https://github.com/MSU-mi231/Unity-3D-Template-2020.3).